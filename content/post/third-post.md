+++
aliases = ["/post/alias-this-url-too", "/post/alias-this-url"]
call_to_action = true
date = 2020-01-16T21:05:53Z
headline = "Model Content the Modular Way"
link_label = "Learn more"
link_url = "https://forestry.io/docs/settings/front-matter-templates/"
slug = ""
support_text = "Modular content modeling with Forestry's Front Matter Templates will boost your productivity, and earn praise from your content editors."
title = "third post!?"
url-features = true

+++
Here's a third post, created by Forestry and modular front matter templates. Headless, indeed!

This post has two aliased URLs, thanks to Hugo's URL management and the c-url front matter template we created.

See [/post/alias-this-url-too](/post/alias-this-url-too) and [/post/alias-this-url](/post/alias-this-url).
